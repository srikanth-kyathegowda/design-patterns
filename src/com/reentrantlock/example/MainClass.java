package com.reentrantlock.example;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainClass {

	Lock reentrantLock = new ReentrantLock();
	Condition condition = reentrantLock.newCondition();
	
	private void producer() throws InterruptedException {
		
		reentrantLock.lock();
		System.out.println("produced...");
		
		condition.await();
		System.out.println("srikanth");
		reentrantLock.unlock();
	}
	
	private void consume() throws InterruptedException {
		Thread.sleep(5000);
		reentrantLock.lock();
		System.out.println("consumed...");
		condition.signal();
		reentrantLock.unlock();
	}
	
	public static void main(String[] args) {
		
		final MainClass m = new MainClass();
		
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					m.producer();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();
	
	new Thread(new Runnable() {
		
		@Override
		public void run() {
			try {
				m.consume();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}).start();
}
	
	
}
