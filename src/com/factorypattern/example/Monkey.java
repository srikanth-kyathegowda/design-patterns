package com.factorypattern.example;

public class Monkey implements Animal {

	@Override
	public void eat() {
		System.out.println("monkey eating...");
	}

}
