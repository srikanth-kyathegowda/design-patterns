package com.factorypattern.example;

public interface Animal {

	public abstract void eat();
	
}
