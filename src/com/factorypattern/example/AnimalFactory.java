package com.factorypattern.example;

public class AnimalFactory {

	public static Animal getAnimal(String type) {
		if(type.equals("DOG" )) {
			return new Dog();
		} else if(type.equals("CAT" )) {
			return new Cat();
		} else if(type.equals("MONKEY" )) {
			return new Monkey();
		} else {
			return null;
		}
	}
	
	public static void main(String[] args) {
		AnimalFactory.getAnimal("DOG").eat();
		AnimalFactory.getAnimal("CAT").eat();
		AnimalFactory.getAnimal("MONKEY").eat();
	}

}
