package com.factorypattern.example;

public class Dog implements Animal {

	@Override
	public void eat() {
		System.out.println("dog eating...");
		
	}

}
