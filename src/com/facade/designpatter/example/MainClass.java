package com.facade.designpatter.example;

public class MainClass {

	public static void main(String[] args) {
		ShapeMaker sm = new ShapeMaker();
		sm.drawCircle();
		sm.drawTriangle();
	}
}
