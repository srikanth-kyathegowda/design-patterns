package com.facade.designpatter.example;

public class ShapeMaker {

	private Shape circle;
	private Shape triangle;
	
	public ShapeMaker() {
		circle = new Circle();
		triangle = new Triangle();
	}
	
	public void drawCircle() {
		circle.draw();
	}
	
	public void drawTriangle() {
		triangle.draw();
	}
	
	
	
}
