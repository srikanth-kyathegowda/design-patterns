package com.facade.designpatter.example;

public interface Shape {

	public abstract void draw();
}
