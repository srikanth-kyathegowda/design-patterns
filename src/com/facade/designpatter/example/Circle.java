package com.facade.designpatter.example;

public class Circle implements Shape {

	@Override
	public void draw() {
		System.out.println("draw circle");
	}

}
