package com.decoratorpattern.example;

public abstract class Decorator implements Temple {

	Temple temple;
	public Decorator(Temple temple) {
		this.temple = temple;
	}
	
	@Override
	public Temple buildTemple() {
		// TODO Auto-generated method stub
		return temple.buildTemple();
	}
}
