package com.decoratorpattern.example;

public class MainClass {

	public static void main(String[] args) {
		Temple temple = new HTemple();
		temple.buildTemple();
		System.out.println(temple);
		
		FlowerDecorator fd = new FlowerDecorator(temple);
		fd.buildTemple();
		System.out.println(fd.temple);
	}
}
