package com.decoratorpattern.example;

public class FlowerDecorator extends Decorator {

	public FlowerDecorator(Temple temple) {
		super(temple);
	}
	
	@Override
	public Temple buildTemple() {
		HTemple ht = (HTemple) temple;
		ht.setFlowers("jasmine");
		return ht;
	}

}
