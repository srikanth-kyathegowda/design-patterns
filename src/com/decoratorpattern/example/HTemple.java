package com.decoratorpattern.example;

public class HTemple implements Temple {

	private String name;
	private String flowers;

	@Override
	public Temple buildTemple() {
		this.name = "jai shri ram";	
		return this;
	}

	@Override
	public String toString() {
		return "HTemple [name=" + name + ", flowers=" + flowers + "]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFlowers() {
		return flowers;
	}

	public void setFlowers(String flowers) {
		this.flowers = flowers;
	}

}
