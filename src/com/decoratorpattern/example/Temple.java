package com.decoratorpattern.example;

public interface Temple {
public abstract Temple buildTemple();
}
