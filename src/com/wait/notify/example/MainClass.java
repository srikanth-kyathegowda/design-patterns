package com.wait.notify.example;

public class MainClass {

	private void produce() throws InterruptedException {

		synchronized (this) {
			System.out.println("produced...");
			wait();
			System.out.println("completed");
		}
	}

	private void consume() throws InterruptedException {

		Thread.sleep(5000);
		synchronized (this) {
			System.out.println("consumed...");
			notify();
		}
	}

	
	public static void main(String[] args) {
		
		final MainClass m = new MainClass();
		
	new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					m.consume();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
	
	
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				try {
					m.produce();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
			}
		}).start();
		
	
		
	}
}
