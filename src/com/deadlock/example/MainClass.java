package com.deadlock.example;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class MainClass {

	Lock lock1 = new ReentrantLock();
	Lock lock2 = new ReentrantLock();

	private void display1() throws InterruptedException {
		lock1.lock();
		Thread.sleep(1000);
		lock2.lock();
		System.out.println("display 1");
		lock2.unlock();
		lock1.unlock();
	}

	private void display2() throws InterruptedException {
		lock2.lock();
		Thread.sleep(1000);
		lock1.lock();
		System.out.println("display2 ");
		lock1.unlock();
		lock2.lock();
	}


	public static void main(String[] args) {

		final MainClass m = new MainClass();
		new Thread(new Runnable() {
			public void run() {
				try {
					m.display1();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}).start();

		new Thread(new Runnable() {
			public void run() {
				try {
					m.display2();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}).start();
	}
}

