package com.stratergy.pattern;

public class MainClass {

	public static void main(String[] args) {
		StratergyManager sm  = new StratergyManager(new Cat());
		sm.eat();
		
		sm  = new StratergyManager(new Dog());
		sm.eat();
	}
}
