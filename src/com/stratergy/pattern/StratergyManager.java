package com.stratergy.pattern;

public class StratergyManager implements Animal {

	private Animal animal;
	
	public StratergyManager(Animal animal) {
		this.animal = animal;
	}

	@Override
	public void eat() {
		this.animal.eat();
	}
	
	
	
}
