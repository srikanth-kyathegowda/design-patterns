package com.stratergy.pattern;

public interface Animal {

	public abstract void eat();
}
