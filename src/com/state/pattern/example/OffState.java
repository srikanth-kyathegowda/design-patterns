package com.state.pattern.example;

public class OffState implements State {

	@Override
	public void doAction(Context context) {
		// TODO Auto-generated method stub
		context.setState(this);
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "from Off State";
	}
}
