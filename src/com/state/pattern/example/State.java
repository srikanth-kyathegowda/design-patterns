package com.state.pattern.example;

public interface State {

	public abstract void doAction(Context context);
}
