package com.state.pattern.example;

public class OnState implements State {

	@Override
	public void doAction(Context context) {
		context.setState(this);
	}

	
	@Override
	public String toString() {
		return "from On State";
	}
	
}
