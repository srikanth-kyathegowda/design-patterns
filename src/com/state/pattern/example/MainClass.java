package com.state.pattern.example;

public class MainClass {

	
	public static void main(String[] args) {
		Context context = new Context();
		OnState on = new OnState();
		on.doAction(context);
		System.out.println(context.getState().toString());
		
		OffState off = new OffState();
		off.doAction(context);
		System.out.println(context.getState().toString());
	}
}
