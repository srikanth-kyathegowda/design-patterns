package com.adopter.design.pattern.example;

public interface Flyable {

	public abstract void fly();
	
}
