package com.adopter.design.pattern.example;

public class Parrot implements Flyable {

	public void fly() {
		System.out.println("parrot flying");
		
	}

}
