package com.adopter.design.pattern.example;

public class Dove implements Flyable {

	public void fly() {
		System.out.println("Dove flying");
		
	}

}
