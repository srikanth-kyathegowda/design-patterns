package com.adopter.design.pattern.example;

public class ElephantAdopter implements Flyable {

	
	private Elephant elephant;
	
	public ElephantAdopter(Elephant elephant) {
		this.elephant = elephant;
	}
	
	public void fly() {
		elephant.walk();
		
	}

}
