package com.adopter.design.pattern.example;

public class MainClass {

	public static void main(String[] args) {
	Flyable dove = new Dove();
	dove.fly();
	Flyable parrot = new Parrot();
	parrot.fly();
	Flyable elephant = new ElephantAdopter(new Elephant());
	elephant.fly();
	}
}
