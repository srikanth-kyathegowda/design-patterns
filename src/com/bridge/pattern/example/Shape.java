package com.bridge.pattern.example;

public abstract class Shape {

	protected Color color;
	
	public abstract void draw();
	
	public abstract void applyColor();
}
