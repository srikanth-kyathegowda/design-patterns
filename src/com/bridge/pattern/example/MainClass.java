package com.bridge.pattern.example;

public class MainClass {

	public static void main(String[] args) {
		
		Shape shape = new Triangle(new RedColor());
		shape.draw();
	}
}
