package com.bridge.pattern.example;

public class Triangle extends Shape {


	public Triangle(Color c) {
		this.color = c;
	}

	@Override
	public void draw() {
		System.out.println("triangle");
		applyColor();
	}

	@Override
	public void applyColor() {
		color.applyColor();
	}

}
