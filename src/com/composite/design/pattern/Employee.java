package com.composite.design.pattern;

import java.util.ArrayList;
import java.util.List;

public class Employee {

	private String name;
	private String designation;

	private List<Employee> subordinates = new ArrayList<Employee>();

	public Employee(String name, String designation) {
		this.designation = designation;
		this.name = name;
	}

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}



	public static void main(String[] args) {
		Employee manager = new Employee("sri", "MNGR");
		Employee sub1 = new Employee("sri", "SUB");
		Employee sub2 = new Employee("sri", "SUB");

		manager.subordinates.add(sub1);
		manager.subordinates.add(sub2);
	}

	public List<Employee> getSubordinates() {
		return subordinates;
	}

	public void setSubordinates(List<Employee> subordinates) {
		this.subordinates = subordinates;
	}
}
