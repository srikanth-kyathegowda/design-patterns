package com.countdown.latch.example;

import java.util.concurrent.CountDownLatch;

class RunTask implements Runnable {

	CountDownLatch countDownLatch;

	public RunTask(CountDownLatch countDownLatch) {
		this.countDownLatch = countDownLatch;
	}

	@Override
	public void run() {
		System.out.println("---job running---");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		countDownLatch.countDown();
	}
}


public class MainClass {

	public static void main(String[] args) {

		CountDownLatch countDownLatch = new CountDownLatch(3);
		Thread t = new Thread(new RunTask(countDownLatch));
		t.start();

		t = new Thread(new RunTask(countDownLatch));
		t.start();

		t = new Thread(new RunTask(countDownLatch));
		t.start();

		try {
			countDownLatch.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println("main completed");
	}
}
