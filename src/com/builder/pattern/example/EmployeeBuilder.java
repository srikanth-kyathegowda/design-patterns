package com.builder.pattern.example;

public class EmployeeBuilder {

	private String name;
	private int age;
	private long saraly;
	
	public EmployeeBuilder setName(String name) {
		this.name = name;
		return this;
	}
	public EmployeeBuilder setAge(int age) {
		this.age = age;
		return this;
	}
	public EmployeeBuilder setSaraly(long saraly) {
		this.saraly = saraly;
		return this;
	}
	
	public Employee getEmployee() {
		return new Employee(name, age, saraly);
	}
	
}
