package com.builder.pattern.example;

public class Employee {

	//it is not a good idea to pass all the required parameter to constructors, instead set only required parameters
	private String name;
	private int age;
	private long saraly;
	
	public Employee(String name, int age, long saraly) {
		super();
		this.name = name;
		this.age = age;
		this.saraly = saraly;
	}

	public static void main(String[] args) {
		Employee e = new EmployeeBuilder().setAge(27).setName("srikanth").getEmployee();//not passing salary here
		System.out.println(e);
	}

	@Override
	public String toString() {
		return "Employee [name=" + name + ", age=" + age + ", saraly=" + saraly
				+ "]";
	}
}
