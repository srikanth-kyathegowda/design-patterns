package com.templatepattern.example;

public abstract class Animal {

	public abstract void wakeUp();
	public abstract void seeGod();
	public abstract void haveBreakFast();
	public abstract void washHands();
	
	public final void templateMethod() {/*Template method to outside world, no one can override*/
		wakeUp();
		seeGod();
		haveBreakFast();
		washHands();
	}
}
