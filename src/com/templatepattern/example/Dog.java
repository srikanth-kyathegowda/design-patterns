package com.templatepattern.example;

public class Dog extends Animal {

	@Override
	public void wakeUp() {
		System.out.println("wakeUp");
		
	}

	@Override
	public void seeGod() {
		System.out.println("seeGod");
		
	}

	@Override
	public void haveBreakFast() {
		System.out.println("haveBreakFast");
		
	}

	@Override
	public void washHands() {
		System.out.println("washHands");
		
	}
	
	
	public static void main(String[] args) {
		Animal a  = new Dog();
		a.templateMethod();
	}

}
