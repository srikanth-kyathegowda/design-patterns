package com.stratergypattern.example;

public class MainClass {

	public static void main(String[] args) {
		TravelStratergy ts = new TravelStratergy(new Bus());
		ts.travelBy();
		
		ts = new TravelStratergy(new Bike());
		ts.travelBy();
	}
}
