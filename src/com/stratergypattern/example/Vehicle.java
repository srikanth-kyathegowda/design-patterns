package com.stratergypattern.example;

public interface Vehicle {

	public abstract void travelBy();
}
