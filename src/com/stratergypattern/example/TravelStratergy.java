package com.stratergypattern.example;

public class TravelStratergy implements Vehicle {
	
	private Vehicle vehical;
	
	public TravelStratergy(Vehicle vehical) {
		this.vehical = vehical;
	}
	
	public void travelBy() {
		this.vehical.travelBy();
		
	}

}
