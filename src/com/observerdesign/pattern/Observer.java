package com.observerdesign.pattern;

public interface Observer {
	public abstract void update(String productName);
}
