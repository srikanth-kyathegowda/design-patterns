package com.observerdesign.pattern;

public class Customer implements Observer {

	private String name;

	@Override
	public void update(String productName) {
		System.out.println("product is available: " + productName);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
