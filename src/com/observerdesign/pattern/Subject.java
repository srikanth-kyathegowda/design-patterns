package com.observerdesign.pattern;

public interface Subject {

	public abstract void subscribeUser(Observer observer);
	public abstract void unsubscribeUser(Observer observer);
	public abstract void notifyUsers();
}
