package com.observerdesign.pattern;

public class MainClass {

	public static void main(String[] args) {
		Product p = new Product();
		p.setName("KS");
		
		p.subscribeUser(new Customer());
		p.subscribeUser(new Customer());
		p.setAvailalbe();
	}
}
