package com.observerdesign.pattern;

import java.util.ArrayList;
import java.util.List;

public class Product implements Subject {

	private String name;
	
	private List<Observer> observers = new ArrayList<Observer>();

	public List<Observer> getObservers() {
		return observers;
	}
	
	public void setAvailalbe() {
		notifyUsers();
	}

	public void setObservers(List<Observer> observers) {
		this.observers = observers;
	}

	@Override
	public void subscribeUser(Observer observer) {
		observers.add(observer);
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void unsubscribeUser(Observer observer) {
		observers.remove(observer);
		
	}

	@Override
	public void notifyUsers() {
		for(Observer observer : observers) {
			observer.update(this.name);
		}
		
	}
	
}
