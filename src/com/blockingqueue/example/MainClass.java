package com.blockingqueue.example;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;



public class MainClass {

	private static BlockingQueue<Integer> queue = new ArrayBlockingQueue<Integer>(5);
	
	private static void producer() {
		
		for(int i=0;i<100;i++) {
			try {
				System.out.println("trying to put item " + i);
				queue.put(i);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static void consume() {
		try {
			for(;;) {
			System.out.println("Item taken: " + queue.take());
			Thread.sleep(10000);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	private static void displayQueueSize() {
		for(;;) {
		System.out.println( "Queue size is " + queue.size());
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		}
	}
	public static void main(String[] args) {
		  new  Thread(new Runnable() {
			
			@Override
			public void run() {
				producer();
			}
		}).start();
		  
		  new  Thread(new Runnable() {
				
			@Override
			public void run() {
			displayQueueSize();
			}
		}).start();
		  
		  new  Thread(new Runnable() {
				
			@Override
			public void run() {
			consume();
			}
		}).start();
	}
}
