package com.visitor.pattern;

public class MainClass {

	public static void main(String[] args) {
		Employee emp = new Employee();
		emp.accept(new OnsiteVisitor());
	}
}
