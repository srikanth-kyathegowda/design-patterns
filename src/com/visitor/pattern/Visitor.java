package com.visitor.pattern;

public interface Visitor {

	public abstract void visit(Employee acceptInterface);
}
