package com.visitor.pattern;

public interface AcceptInterface {

	
	public abstract void accept(Visitor visitor);
}
