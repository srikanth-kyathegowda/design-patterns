package com.chainofresponsibilities.example;

public interface Chain {

	public abstract void resolveRequest(Request request);
	
}
