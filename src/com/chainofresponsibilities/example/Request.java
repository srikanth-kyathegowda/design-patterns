package com.chainofresponsibilities.example;

public class Request {

	private String requestType;
	public Request(String type) {
		this.requestType = type;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}
	
	
}
