package com.chainofresponsibilities.example;

public class Employee implements Chain {

	Chain nextChain;
	@Override
	public void resolveRequest(Request request) {
		if(request.getRequestType().equals("LOW")) {
			System.out.println("employee can handle");
		} else {
			nextChain.resolveRequest(request);
		}
		
	}

}
