package com.chainofresponsibilities.example;

public class Manager implements Chain {

	Chain nextChain;
	
	@Override
	public void resolveRequest(Request request) {
		if(request.getRequestType().equals("MEDIUM")) {
			System.out.println("Manager can handle");
		} else {
			nextChain.resolveRequest(request);

		}

	}
}