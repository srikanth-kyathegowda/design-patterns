package com.chainofresponsibilities.example;

public class Main {

	public static void main(String[] args) {
		Request request = new Request("LO");
		Employee e = new Employee();
		Manager m = new Manager();
		Director d = new Director();
		e.nextChain = m;
		m.nextChain = d;
		e.resolveRequest(request);
	}
}
