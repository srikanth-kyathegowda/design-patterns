package com.chainofresponsibilities.example;

public class Director implements Chain {

	Chain nextChain;
	
	@Override
	public void resolveRequest(Request request) {

		if(request.getRequestType().equals("HIGH")) {
			System.out.println("direcctor can handle");
		} else {
			System.out.println("nobody in the chain can handle this request");
		}

	}

}
