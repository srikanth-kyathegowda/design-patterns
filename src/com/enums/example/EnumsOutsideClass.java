package com.enums.example;

public class EnumsOutsideClass {

	public static void main(String[] args) {
	BottleSize bs = BottleSize.BIG;	
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("doe");
		super.finalize();
	}
}

 	

/*if the enum is present outside the class, the class rules applies i.e we can use default / public access only*/
enum BottleSize {
	BIG, SMALL
}

//objects can become eligible for garbage collection even if they have valid references, this is called isolated references.