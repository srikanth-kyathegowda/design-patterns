package com.enums.example;

public class EnumWithConstructor {

	enum PenColor {
		RED(45), GREEN(90){public int getPrice() {
			return 67;
		}};		/*Enums can have constructor, but must be private modifier.
		of no access mdifier is present it will treat as private not as default*/
		PenColor(int price) {
			this.price = price;
		}
		private int price;
		public int getPrice() {
			return price;
		}
		
		
	}
	
	
	private Object returnArray() {
		
		byte b = 9;
		long y = 40;
		int x = (int) y;
		
		float[] array = new float[90];
		return array;
		
	}
	
}
