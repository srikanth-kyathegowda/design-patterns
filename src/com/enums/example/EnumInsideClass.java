package com.enums.example;

public class EnumInsideClass {

	private static final String x;
	
	static {
		x = "";
	}
	
	public enum PenSize {
		BIG, SMALL
	}
	
	public static void main(String[] args) {
	Integer i = new Integer(1);
	Integer j = i;
	j++;
	System.out.println(i == j);
	Boolean b = true;
	Boolean c = true;
	System.out.println(b == c);
	}
	
}
