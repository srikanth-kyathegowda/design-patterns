package com.decorator.design.pattern;

public class Cat implements Animal {

	@Override
	public String toString() {
		return "Cat [name=" + name + ", necklace=" + necklace + "]";
	}
	private String name;
	private String necklace;
	
	@Override
	public Animal createAnimal() {
		this.name = "my cat";// do not set cecklace, decorator will take care of this
		return this;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNecklace() {
		return necklace;
	}
	public void setNecklace(String necklace) {
		this.necklace = necklace;
	}
	
	
	
	
}
