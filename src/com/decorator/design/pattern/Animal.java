package com.decorator.design.pattern;

public interface Animal {

	public abstract Animal createAnimal();
	
}
