package com.decorator.design.pattern;

public class NecklaceDecorator extends Decorator {

	public NecklaceDecorator(Animal animal) {
		super(animal);
	}
	
	@Override
	public Animal createAnimal() {
		//animal.createAnimal();
		addNecklace(animal);
		return animal;
	}
	
	public void addNecklace(Animal animal) {
		Cat cat = (Cat) animal;
		cat.setNecklace("my necklace");
	}
	
}
