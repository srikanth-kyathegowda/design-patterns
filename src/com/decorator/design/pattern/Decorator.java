package com.decorator.design.pattern;

public abstract class Decorator implements Animal {

	Animal animal;
	
	public Decorator(Animal animal) {
		this.animal = animal;
	}
	
	
	@Override
	public Animal createAnimal() {
		return animal.createAnimal();
	}
	
	
}
