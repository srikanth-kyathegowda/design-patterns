package com.decorator.design.pattern;

public class MainClass {

	public static void main(String[] args) {
		Animal animal = new Cat();
		animal.createAnimal();
		System.out.println(animal);
		// now the necklace is not set
		//let us create decorater to set necklace
		NecklaceDecorator necklaceDecorator = new NecklaceDecorator(animal);
		necklaceDecorator.createAnimal();
		System.out.println(necklaceDecorator.animal);
	}
}
